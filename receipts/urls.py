from django.urls import path

from receipts.views import (
    ExpenseCategoryListView,
    AccountListView,
    ReceiptListView,
    CategoryCreateView,
    AccountCreateView,
    ReceiptCreateView,
)

urlpatterns = [
    path(
        "categories/",
        ExpenseCategoryListView.as_view(),
        name="receiptcategories_list",
    ),
    path("accounts/", AccountListView.as_view(), name="receiptaccounts_list"),
    path("", ReceiptListView.as_view(), name="home"),
    path(
        "categories/create/",
        CategoryCreateView.as_view(),
        name="receiptcategory_create",
    ),
    path(
        "accounts/create/",
        AccountCreateView.as_view(),
        name="receiptaccount_create",
    ),
    path("create/", ReceiptCreateView.as_view(), name="receipt_create"),
]
