from django.shortcuts import render, redirect
from django.urls import reverse_lazy

# from django.views.generic.base import TemplateView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView


from django.contrib.auth.mixins import LoginRequiredMixin

from receipts.models import Account, ExpenseCategory, Receipt


# Create your views here.
class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receipts/list.html"

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)


class ExpenseCategoryListView(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    template_name = "receiptcategories/list.html"

    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)


class AccountListView(LoginRequiredMixin, ListView):
    model = Account
    template_name = "receiptaccounts/list.html"

    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)


class CategoryCreateView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    template_name = "receiptcategories/new.html"
    fields = ["name"]
    success_url = reverse_lazy("home")

    def form_valid(self, form):
        category = form.save(commit=False)
        category.owner = self.request.user
        category.save()
        return redirect("home")


class AccountCreateView(LoginRequiredMixin, CreateView):
    model = Account
    template_name = "receiptaccounts/new.html"
    fields = ["name", "number"]
    success_url = reverse_lazy("home")

    def form_valid(self, form):
        account = form.save(commit=False)
        account.owner = self.request.user
        account.save()
        return redirect("home")


class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipt
    template_name = "receipts/new.html"
    # QUESTION:
    # do "category" and "account" go inside of the fields? not sure...
    # I'd assume they don't since they don't directly take an input...
    # these fields are supposed to be created via the CategoryCreateView and AccountCreateView?
    # we'll see.
    # ANSWER (AFTER TESTING IT):
    # yes!! "category" and "account" do go inside the fields below.
    # they show up as dropdowns.
    fields = ["vendor", "total", "tax", "date", "category", "account"]
    success_url = reverse_lazy("home")

    def form_valid(self, form):
        receipt = form.save(commit=False)
        receipt.purchaser = self.request.user
        receipt.save()
        return redirect("home")
